FROM ruby:2.4.2-alpine

ENV BUNDLER_VERSION=2.0.1

RUN echo 'gem: --no-document' > .gemrc && \
  echo 'http://dl-4.alpinelinux.org/alpine/v3.4/main' >> /etc/apk/repositories && \
  echo 'http://dl-4.alpinelinux.org/alpine/v3.4/community' >> /etc/apk/repositories

RUN apk add --update \
    gcc \
    curl \
    unzip \
    nodejs \
    abuild \
    tzdata \
    binutils \
    make cmake \
    build-base \
    postgresql-dev \
    chromium chromium-chromedriver \
    musl-dev libexif libxslt-dev libxml2-dev && \
  rm -rf /var/cache/apk/* && \
  gem install bundler -v $BUNDLER_VERSION --no-document
